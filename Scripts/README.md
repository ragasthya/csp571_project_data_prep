# CSP571 Project Data Preparation
## Authors:
A. Ajay Roopesh Mohan   A20435463   amohan6@hawk.iit.edu    
B. Jay Rodge	        A20418613   jrodge@hawk.iit.edu     
C. Manasdeep Deb        A20449643   mdeb@hawk.iit.edu   
D. Nicholas Tating      A20452101   ntating@hawk.iit.edu    
E. Rahul Agasthya       A20428452   ragasthya@hawk.iit.edu


## About:
This includes python code to clean and merge csv files and geojson files for the CSP 571 Lead Crime Analysis Project.

## Running of the Scripts:
1. Run `crime_data_prep.py`. This will get the `training.csv` files in `Output` folder.
2. Run `get_test_data.py`. This will get the `test.csv` files in `Output` folder.
3. Run `exploratory_analysis.py`. This will show the various charts for the training data. 
4. Run `TrainingModelAnalysis.R` which will show the accuracy and ROC curves for the training data.
5. Run `ClassifierOutput.R` which will get the predictions for the test data.
6. Run `output_to_geojson.py` which will generate the GeoJson data for the predictions.
