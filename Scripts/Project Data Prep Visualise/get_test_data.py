import csv
import json
from classes import OUTPUT_LOCATION, TEST_OUTPUT, COUNTY_COORDINATE_DATA, CrimeRecordEntry, LeadRecordEntry, Entry, County, test_lead_years


lead_entries = {}

for year in test_lead_years:
    file = test_lead_years[year]
    index = 0
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            check = ''
            if check.join(row) == '':
                break
            if not index == 0:
                year = year
                if year == 1993 or year == 1994 or year == 1995 or year == 1996:
                    county = str(row[4].strip()).upper()
                    total_tested = int(row[5].replace('--', '0').replace(',', '').strip())
                    child_population = int(row[5].replace('--', '0').replace(',', '').strip())
                    low_lead = int(row[0].replace('--', '0').replace(',', '').strip())
                    medium_lead = int(row[1].replace('--', '0').replace(',', '').strip())
                    high_lead = int(row[2].replace('--', '0').replace(',', '').strip())
                else:
                    county = str(row[6].strip()).upper()
                    total_tested = int(row[7].replace('--', '0').replace(',', '').strip())
                    child_population = int(row[2].replace('--', '0').replace(',', '').split('.')[0].strip())
                    low_lead = int(row[0].replace('--', '0').replace(',', '').strip()) + int(
                        row[1].replace('--', '0').replace(',', '').strip())
                    medium_lead = int(row[3].replace('--', '0').replace(',', '').strip()) + int(
                        row[4].replace('--', '0').replace(',', '').strip())
                    high_lead = int(row[5].replace('--', '0').replace(',', '').strip())
                if county == 'COOK W/O CHICAGO':
                    county = 'COOK'
                elif county == 'DEKALB':
                    county = 'DE KALB'
                elif county == 'DUPAGE':
                    county = 'DU PAGE'
                elif county == 'JODAVIESS':
                    county = 'JO DAVIESS'
                elif county == 'DEWITT':
                    county = 'DE WITT'
                elif county == 'LASALLE':
                    county = 'LA SALLE'
                if county in lead_entries:
                    entry_dict = lead_entries[county]
                else:
                    entry_dict = {}
                new_entry = LeadRecordEntry(year, county, total_tested, child_population, low_lead, medium_lead,
                                            high_lead)
                entry_dict[year] = new_entry
                lead_entries[county] = entry_dict
            index += 1

chicago_data = lead_entries['CHICAGO']
cook_data = lead_entries['COOK']

for year in chicago_data:
    cook_entry = cook_data[year]
    chicago_entry = chicago_data[year]
    cook_entry.add_another_county(chicago_entry)
    cook_data[year] = cook_entry

del lead_entries["CHICAGO"]

entries = {}
for county in lead_entries:
    for year in lead_entries[county]:
        crime_entry = None
        lead_entry = lead_entries[county][year]
        entry = Entry(crime_entry, lead_entry)
        if entry.county in entries:
            entries_list = entries[entry.county]
        else:
            entries_list = []
        entries_list.append(entry)
        entries[entry.county] = entries_list

print(entries)

with open(COUNTY_COORDINATE_DATA) as f:
    data = json.load(f)

for feature in data['features']:
    county = feature['properties']['COUNTY_NAM']
    if county == 'COOK W/O CHICAGO':
        county = 'COOK'
    elif county == 'DEKALB':
        county = 'DE KALB'
    elif county == 'DUPAGE':
        county = 'DU PAGE'
    elif county == 'JODAVIESS':
        county = 'JO DAVIESS'
    elif county == 'DEWITT':
        county = 'DE WITT'
    elif county == 'LASALLE':
        county = 'LA SALLE'
    type = feature['geometry']['type']
    coordinates = feature['geometry']['coordinates']
    entry_list = entries[county]
    for entry in entry_list:
        entry.set_geometry(type, coordinates)

with open(TEST_OUTPUT, mode='w') as out_file:
    out_file_writer = csv.writer(out_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    out_file_writer.writerow(['COUNTY', 'LATITUDE', 'LONGITUDE', 'LEAD TEST YEAR', 'TOTAL TESTED', 'CHILD POPULATION', 'LOW LEAD LEVELS (15-19 mcg/dl)', 'MEDIUM LEAD LEVELS (20-44 mcg/dl)', 'HIGH LEAD LEVELS (>45 mcg/dl)'])

    for county in entries:
        for entry in entries[county]:
            out_file_writer.writerow(entry.to_row())
            print(str(entry))

out_file.close()

years = {}
for county in entries:
    for entry in entries[county]:
        if entry.poisoning_year in years:
            entry_list = years[entry.poisoning_year]
        else:
            entry_list = []
        entry_list.append(entry)
        years[entry.poisoning_year] = entry_list

for year in years:
    file_data = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'urn:ogc:def:crs:OGC:1.3:CRS84'
            }
        },
        'features': []
    }
    for entry in years[year]:
        file_data['features'].append(entry.as_feature())
    print(json.dumps(file_data, indent=4, sort_keys=True))
    with open(OUTPUT_LOCATION + str(year) + '_data.geojson', 'w') as f:
        f.write(json.dumps(file_data, indent=4, sort_keys=True))
