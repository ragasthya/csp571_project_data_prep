import csv
import json
from geojson import Point, Feature, FeatureCollection, dump
from classes import OUTPUT_LOCATION, OUTPUT, COUNTY_COORDINATE_DATA, CrimeRecordEntry, LeadRecordEntry, Entry, County, crime_years, lead_years, year_match

crime_entries = {}

for year in crime_years:
    file = crime_years[year]
    index = 0
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            check = ''
            if check.join(row) == '':
                break
            if not index == 0:
                year = year
                if year == 2013:
                    county = str(row[0].strip()).upper()
                    agency = str(row[1].strip()).upper()
                    population = int(row[3].strip())
                    total_crimes = int(row[6].replace('--', '0').strip())
                    crime_rate = float(row[9].replace('--', '0').replace(',', '').strip())
                    homicide = int(row[12].replace('--', '0').strip())
                    rape = int(row[15].replace('--', '0').strip())
                    robbery = int(row[18].replace('--', '0').strip())
                    assault = int(row[21].replace('--', '0').strip())
                    burglary = int(row[24].replace('--', '0').strip()) + int(row[27].replace('--', '0').strip()) + int(
                        row[30].replace('--', '0').strip())
                    arson = int(row[33].replace('--', '0').strip())
                    human_trafficking = 0
                    total_arrests = int(row[38].replace('--', '0').strip())
                    arrest_rate = float(row[41].replace('--', '0').replace(',', '').strip())
                elif year == 2014:
                    county = str(row[0].strip()).upper()
                    agency = str(row[1].strip()).upper()
                    population = int(row[2].strip())
                    total_crimes = int(row[5].replace('--', '0').strip())
                    crime_rate = float(row[8].replace('--', '0').replace(',', '').strip())
                    homicide = int(row[11].replace('--', '0').strip())
                    rape = int(row[14].replace('--', '0').strip())
                    robbery = int(row[17].replace('--', '0').strip())
                    assault = int(row[20].replace('--', '0').strip())
                    burglary = int(row[23].replace('--', '0').strip()) + int(row[26].replace('--', '0').strip()) + int(
                        row[29].replace('--', '0').strip())
                    arson = int(row[32].replace('--', '0').strip())
                    human_trafficking = int(row[32].replace('--', '0').strip()) + int(
                        row[35].replace('--', '0').strip())
                    total_arrests = int(row[37].replace('--', '0').strip())
                    arrest_rate = float(row[40].replace('--', '0').replace(',', '').strip())
                else:
                    county = str(row[1].strip()).upper()
                    agency = str(row[0].strip()).upper()
                    population = int(row[2].strip())
                    total_crimes = int(row[4].replace('--', '0').strip())
                    crime_rate = float(row[6].replace('--', '0').replace(',', '').strip())
                    homicide = int(row[8].replace('--', '0').strip())
                    rape = int(row[10].replace('--', '0').strip())
                    robbery = int(row[12].replace('--', '0').strip())
                    assault = int(row[14].replace('--', '0').strip())
                    burglary = int(row[16].replace('--', '0').strip()) + int(row[18].replace('--', '0').strip()) + int(
                        row[20].replace('--', '0').strip())
                    arson = int(row[22].replace('--', '0').strip())
                    human_trafficking = int(row[24].replace('--', '0').strip()) + int(
                        row[26].replace('--', '0').strip())
                    total_arrests = int(row[28].replace('--', '0').strip())
                    arrest_rate = float(row[30].replace('--', '0').replace(',', '').strip())
                if len(agency) == 0:
                    if county == 'LASALLE':
                        county = 'LA SALLE'
                    elif county == 'DEWITT':
                        county = 'DE WITT'
                    if county in crime_entries:
                        entry_dict = crime_entries[county]
                    else:
                        entry_dict = {}
                    new_entry = CrimeRecordEntry(year, county, population, total_crimes, crime_rate, homicide, rape,
                                                 robbery, assault, burglary, arson, human_trafficking, total_arrests,
                                                 arrest_rate)
                    entry_dict[year] = new_entry
                    crime_entries[county] = entry_dict
            index += 1

lead_entries = {}

for year in lead_years:
    file = lead_years[year]
    index = 0
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for row in csv_reader:
            check = ''
            if check.join(row) == '':
                break
            if not index == 0:
                year = year
                # county, total_tested, child_population, low_lead, medium_lead, high_lead
                if year == 1993 or year == 1994 or year == 1995 or year == 1996:
                    county = str(row[4].strip()).upper()
                    total_tested = int(row[5].replace('--', '0').replace(',', '').strip())
                    child_population = int(row[5].replace('--', '0').replace(',', '').strip())
                    low_lead = int(row[0].replace('--', '0').replace(',', '').strip())
                    medium_lead = int(row[1].replace('--', '0').replace(',', '').strip())
                    high_lead = int(row[2].replace('--', '0').replace(',', '').strip())
                else:
                    county = str(row[6].strip()).upper()
                    total_tested = int(row[7].replace('--', '0').replace(',', '').strip())
                    child_population = int(row[2].replace('--', '0').replace(',', '').split('.')[0].strip())
                    low_lead = int(row[0].replace('--', '0').replace(',', '').strip()) + int(
                        row[1].replace('--', '0').replace(',', '').strip())
                    medium_lead = int(row[3].replace('--', '0').replace(',', '').strip()) + int(
                        row[4].replace('--', '0').replace(',', '').strip())
                    high_lead = int(row[5].replace('--', '0').replace(',', '').strip())
                if county == 'COOK W/O CHICAGO':
                    county = 'COOK'
                elif county == 'DEKALB':
                    county = 'DE KALB'
                elif county == 'DUPAGE':
                    county = 'DU PAGE'
                elif county == 'JODAVIESS':
                    county = 'JO DAVIESS'
                elif county == 'DEWITT':
                    county = 'DE WITT'
                elif county == 'LASALLE':
                    county = 'LA SALLE'
                if county in lead_entries:
                    entry_dict = lead_entries[county]
                else:
                    entry_dict = {}
                new_entry = LeadRecordEntry(year, county, total_tested, child_population, low_lead, medium_lead,
                                            high_lead)
                entry_dict[year] = new_entry
                lead_entries[county] = entry_dict
            index += 1

chicago_data = lead_entries['CHICAGO']
cook_data = lead_entries['COOK']

for year in chicago_data:
    cook_entry = cook_data[year]
    chicago_entry = chicago_data[year]
    cook_entry.add_another_county(chicago_entry)
    cook_data[year] = cook_entry

del lead_entries["CHICAGO"]

entries = {}
for county in crime_entries:
    for year in crime_entries[county]:
        crime_entry = crime_entries[county][year]
        lead_entry = lead_entries[county][year_match[year]]
        entry = Entry(crime_entry, lead_entry)
        if entry.county in entries:
            entries_list = entries[entry.county]
        else:
            entries_list = []
        entries_list.append(entry)
        entries[entry.county] = entries_list


with open(COUNTY_COORDINATE_DATA) as f:
    data = json.load(f)

for feature in data['features']:
    county = feature['properties']['COUNTY_NAM']
    if county == 'COOK W/O CHICAGO':
        county = 'COOK'
    elif county == 'DEKALB':
        county = 'DE KALB'
    elif county == 'DUPAGE':
        county = 'DU PAGE'
    elif county == 'JODAVIESS':
        county = 'JO DAVIESS'
    elif county == 'DEWITT':
        county = 'DE WITT'
    elif county == 'LASALLE':
        county = 'LA SALLE'
    type = feature['geometry']['type']
    coordinates = feature['geometry']['coordinates']
    entry_list = entries[county]
    for entry in entry_list:
        entry.set_geometry(type, coordinates)

with open(OUTPUT, mode='w') as out_file:
    out_file_writer = csv.writer(out_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    out_file_writer.writerow(['COUNTY', 'LATITUDE', 'LONGITUDE', 'CRIME YEAR', 'POPULATION', 'TOTAL CRIMES', 'CRIME RATE', 'HOMICIDE', 'RAPE', 'ROBBERY', 'ASSAULT', 'BURGLARY', 'ARSON', 'HUMAN TRAFFICKING', 'TOTAL INCARCERATION', 'INCARCERATION RATE', 'LEAD TEST YEAR', 'TOTAL TESTED', 'CHILD POPULATION', 'LOW LEAD LEVELS (15-19 mcg/dl)', 'MEDIUM LEAD LEVELS (20-44 mcg/dl)', 'HIGH LEAD LEVELS (>45 mcg/dl)'])

    for county in entries:
        for entry in entries[county]:
            out_file_writer.writerow(entry.to_row())
            print(str(entry))

out_file.close()

years = {}
counties = {}
for county in entries:
    for entry in entries[county]:
        counties[county] = County(county, entry.latitude, entry.longitude)
        if entry.crime_year in years:
            entry_list = years[entry.crime_year]
        else:
            entry_list = []
        entry_list.append(entry)
        years[entry.crime_year] = entry_list

features = []
for county in counties:
    features.append(counties[county].as_feature())

feature_collection = FeatureCollection(features)
with open('illinois_counties.geojson', 'w') as f:
   dump(feature_collection, f, indent=4, sort_keys=True)

for year in years:
    file_data = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'urn:ogc:def:crs:OGC:1.3:CRS84'
            }
        },
        'features': []
    }
    for entry in years[year]:
        file_data['features'].append(entry.as_feature())
    print(json.dumps(file_data, indent=4, sort_keys=True))
    with open(OUTPUT_LOCATION + str(year) + '_data.geojson', 'w') as f:
        f.write(json.dumps(file_data, indent=4, sort_keys=True))
