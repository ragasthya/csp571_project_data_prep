import csv
import json
import itertools
import matplotlib.pyplot as plt


years_match = {
    1993: 2013,
    1994: 2014,
    1995: 2015,
    1996: 2016,
    1997: 2017,
    1998: 2018,
    1999: 2019,
    2000: 2020,
    2001: 2021,
    2002: 2022,
    2003: 2023,
    2004: 2024,
    2005: 2025,
    2006: 2026
}

DATASET_LOCATION = './Datasets/'
OUTPUT_LOCATION = './Output/'
CSV_EXTENSION = '.csv'
GEO_JSON_EXTENSION = '.geojson'

TRAINING = OUTPUT_LOCATION + 'training' + CSV_EXTENSION
TEST = OUTPUT_LOCATION + 'test' + CSV_EXTENSION
TRAINING_OUTPUT = OUTPUT_LOCATION + 'training_R' + CSV_EXTENSION
TEST_OUTPUT = OUTPUT_LOCATION + 'test_R' + CSV_EXTENSION

COUNTY_COORDINATE_DATA = DATASET_LOCATION + 'Illinois_County_Maps' + GEO_JSON_EXTENSION


class Geometry:
    def __init__(self, geometry_type, geometry_coordinates):
        self.geometry_type = geometry_type
        self.geometry_coordinates = geometry_coordinates[0]

    def as_feature(self):
        return {
            "type": self.geometry_type,
            "coordinates": [self.geometry_coordinates]
        }


class TrainingEntry:
    def __init__(self, county, predict_year, population, total_crimes, crime_rate, homicide, rape, robbery, assault, burglary, arson, human_trafficking, total_arrests, arrest_rate, lead_year, total_tested, child_population, low_lead, medium_lead, high_lead, prediction_glm, prediction_tree, prediction_nb, prediction_forest):
        self.county = county
        self.predict_year = predict_year
        self.population = population
        self.total_crimes = total_crimes
        self.crime_rate = crime_rate
        self.homicide = homicide
        self.rape = rape
        self.robbery = robbery
        self.assault = assault
        self.burglary = burglary
        self.arson = arson
        self.human_trafficking = human_trafficking
        self.total_arrests = total_arrests
        self.arrest_rate = arrest_rate
        self.lead_year = lead_year
        self.total_tested = total_tested
        self.child_population = child_population
        self.low_lead = low_lead
        self.medium_lead = medium_lead
        self.high_lead = high_lead
        self.percent_affected = float(low_lead + medium_lead + high_lead) * 100/float(total_tested)
        self.prediction_glm = prediction_glm
        self.prediction_tree = prediction_tree
        self.prediction_nb = prediction_nb
        self.prediction_forest = prediction_forest
        self.geometry = None

    def set_geometry(self, geometry_type, geometry_coordinates):
        self.geometry = Geometry(geometry_type, geometry_coordinates)

    def as_feature(self):
        return {
            "type": "Feature",
            "properties": {
                "county": self.county,
                "predict_year": self.predict_year,
                "poisoning_year": self.lead_year,
                "population": self.population,
                "homicide": self.homicide,
                "rape": self.rape,
                "robbery": self.robbery,
                "assault": self.assault,
                "burglary": self.burglary,
                "arson": self.arson,
                "human_trafficking": self.human_trafficking,
                "crime_rate": self.crime_rate,
                "total_arrests": self.total_arrests,
                "arrest_rate": self.arrest_rate,
                "low_lead": self.low_lead,
                "medium_lead": self.medium_lead,
                "high_lead": self.high_lead,
                "percent_affected": self.percent_affected,
                "prediction_glm": self.prediction_glm,
                "prediction_tree": self.prediction_tree,
                "prediction_nb": self.prediction_nb,
                "prediction_forest": self.prediction_forest,
                "total_poisoned": self.low_lead + self.medium_lead + self.high_lead
            },
            "geometry": self.geometry.as_feature()
        }


class TestEntry:
    def __init__(self, county, predict_year, lead_year, total_tested, child_population, low_lead, medium_lead, high_lead, prediction_glm, prediction_tree, prediction_nb, prediction_forest):
        self.county = county
        self.predict_year = predict_year
        self.lead_year = lead_year
        self.total_tested = total_tested
        self.child_population = child_population
        self.low_lead = low_lead
        self.medium_lead = medium_lead
        self.high_lead = high_lead
        self.percent_affected = float(low_lead + medium_lead + high_lead) * 100/float(total_tested)
        self.prediction_glm = prediction_glm
        self.prediction_tree = prediction_tree
        self.prediction_nb = prediction_nb
        self.prediction_forest = prediction_forest
        self.geometry = None

    def set_geometry(self, geometry_type, geometry_coordinates):
        self.geometry = Geometry(geometry_type, geometry_coordinates)

    def as_feature(self):
        return {
            "type": "Feature",
            "properties": {
                "county": self.county,
                "predict_year": self.predict_year,
                "poisoning_year": self.lead_year,
                "low_lead": self.low_lead,
                "medium_lead": self.medium_lead,
                "high_lead": self.high_lead,
                "percent_affected": self.percent_affected,
                "prediction_glm": self.prediction_glm,
                "prediction_tree": self.prediction_tree,
                "prediction_nb": self.prediction_nb,
                "prediction_forest": self.prediction_forest,
                "total_poisoned": self.low_lead + self.medium_lead + self.high_lead
            },
            "geometry": self.geometry.as_feature()
        }


f1 = open(TRAINING)
f2 = open(TRAINING_OUTPUT)

csv_file_train_reader = csv.reader(f1)
csv_file_train_output_reader = csv.reader(f2)

training_counties_year = {}

index = 0
for row1, row2 in itertools.zip_longest(csv_file_train_reader, csv_file_train_output_reader):
    if index == 0:
        index += 1
        continue
    county = str(row1[0].strip()).upper()
    population = int(row1[4].strip())
    total_crimes = int(row1[5].strip())
    crime_rate = float(row1[6].strip())
    homicide = int(row1[7].strip())
    rape = int(row1[8].strip())
    robbery = int(row1[9].strip())
    assault = int(row1[10].strip())
    burglary = int(row1[11].strip())
    arson = int(row1[12].strip())
    human_trafficking = int(row1[13].strip())
    total_arrests = int(row1[14].strip())
    arrest_rate = float(row1[15].strip())
    lead_year = int(row1[16].strip())
    predict_year = years_match[int(row1[16].strip())]
    total_tested = int(row1[17].strip())
    child_population = int(row1[18].strip())
    low_lead = int(row1[19].strip())
    medium_lead = int(row1[20].strip())
    high_lead = int(row1[21].strip())

    prediction_glm = int(row2[1].strip())
    prediction_tree = int(row2[2].strip())
    prediction_nb = int(row2[3].strip())
    prediction_forest = int(row2[4].strip())

    entry = TrainingEntry(county, predict_year, population, total_crimes, crime_rate, homicide, rape, robbery, assault, burglary, arson, human_trafficking, total_arrests, arrest_rate, lead_year, total_tested, child_population, low_lead, medium_lead, high_lead, prediction_glm, prediction_tree, prediction_nb, prediction_forest)
    if predict_year in training_counties_year:
        counties = training_counties_year[predict_year]
    else:
        counties = {}

    if county in counties:
        entries = counties[county]
    else:
        entries = []
    entries.append(entry)
    counties[county] = entries
    training_counties_year[predict_year] = counties


with open(COUNTY_COORDINATE_DATA) as f:
    data = json.load(f)

for feature in data['features']:
    county = feature['properties']['COUNTY_NAM']
    if county == 'COOK W/O CHICAGO':
        county = 'COOK'
    elif county == 'DEKALB':
        county = 'DE KALB'
    elif county == 'DUPAGE':
        county = 'DU PAGE'
    elif county == 'JODAVIESS':
        county = 'JO DAVIESS'
    elif county == 'DEWITT':
        county = 'DE WITT'
    elif county == 'LASALLE':
        county = 'LA SALLE'
    type = feature['geometry']['type']
    coordinates = feature['geometry']['coordinates']
    for year in training_counties_year:
        counties = training_counties_year[year]
        entry_list = counties[county]
        for entry in entry_list:
            entry.set_geometry(type, coordinates)


f1 = open(TEST)
f2 = open(TEST_OUTPUT)

csv_file_train_reader = csv.reader(f1)
csv_file_train_output_reader = csv.reader(f2)

test_counties_year = {}

index = 0
for row1, row2 in itertools.zip_longest(csv_file_train_reader, csv_file_train_output_reader):
    if index == 0:
        index += 1
        continue
    county = str(row1[0].strip()).upper()
    lead_year = int(row1[3].strip())
    predict_year = years_match[int(row1[3].strip())]
    total_tested = int(row1[4].strip())
    child_population = int(row1[5].strip())
    low_lead = int(row1[6].strip())
    medium_lead = int(row1[7].strip())
    high_lead = int(row1[8].strip())

    prediction_glm = int(row2[1].strip())
    prediction_tree = int(row2[2].strip())
    prediction_nb = int(row2[3].strip())
    prediction_forest = int(row2[4].strip())

    entry = TestEntry(county, predict_year, lead_year, total_tested, child_population, low_lead, medium_lead, high_lead, prediction_glm, prediction_tree, prediction_nb, prediction_forest)
    if predict_year in test_counties_year:
        counties = test_counties_year[predict_year]
    else:
        counties = {}

    if county in counties:
        entries = counties[county]
    else:
        entries = []
    entries.append(entry)
    counties[county] = entries
    test_counties_year[predict_year] = counties

lead_counts = []
crime_counts = []
predicted_counts = []
years = []
for year in training_counties_year:
    count_lead = 0
    count_crime = 0
    count_predicted = 0
    counties = training_counties_year[year]
    for county in counties:
        entry_list = counties[county]
        for entry in entry_list:
            if entry.percent_affected > 0.0:
                count_lead += 1
            if entry.crime_rate > 0.0:
                count_crime += 1
            if entry.prediction_glm > 1.5:
                count_predicted += 1
    years.append(year)
    lead_counts.append(count_lead)
    crime_counts.append(count_crime)
    predicted_counts.append(count_predicted*2.2)

plt.plot(years, lead_counts, 'go')
plt.plot(years, crime_counts, 'ro')
plt.plot(years, predicted_counts, 'bo')
master_list = lead_counts + crime_counts + predicted_counts
print(master_list)
plt.axis([2012, 2019, min(master_list)-10, max(master_list)+10])
plt.show()

lead_counts = []
predicted_counts = []
years = []
for year in test_counties_year:
    count_lead = 0
    count_predicted = 0
    counties = test_counties_year[year]
    for county in counties:
        entry_list = counties[county]
        for entry in entry_list:
            if entry.percent_affected > 0.0:
                count_lead += 1
            if entry.prediction_glm > 1.5:
                count_predicted += 1
    years.append(year)
    lead_counts.append(count_lead)
    predicted_counts.append(count_predicted*2.2)

plt.plot(years, lead_counts, 'go')
plt.plot(years, predicted_counts, 'bo')
master_list = lead_counts + predicted_counts
print(master_list)
plt.axis([min(years)-1, max(years)+1, min(master_list)-10, max(master_list)+10])
plt.show()


with open(COUNTY_COORDINATE_DATA) as f:
    data = json.load(f)

for feature in data['features']:
    county = feature['properties']['COUNTY_NAM']
    if county == 'COOK W/O CHICAGO':
        county = 'COOK'
    elif county == 'DEKALB':
        county = 'DE KALB'
    elif county == 'DUPAGE':
        county = 'DU PAGE'
    elif county == 'JODAVIESS':
        county = 'JO DAVIESS'
    elif county == 'DEWITT':
        county = 'DE WITT'
    elif county == 'LASALLE':
        county = 'LA SALLE'
    type = feature['geometry']['type']
    coordinates = feature['geometry']['coordinates']
    for year in test_counties_year:
        counties = test_counties_year[year]
        entry_list = counties[county]
        for entry in entry_list:
            entry.set_geometry(type, coordinates)


for year in training_counties_year:
    file_data = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'urn:ogc:def:crs:OGC:1.3:CRS84'
            }
        },
        'features': []
    }
    counties = training_counties_year[year]
    for county in counties:
        entry_list = counties[county]
        for entry in entry_list:
            file_data['features'].append(entry.as_feature())
    with open(OUTPUT_LOCATION + str(year) + '_output_data.geojson', 'w') as f:
        f.write(json.dumps(file_data, indent=4, sort_keys=True))

for year in test_counties_year:
    file_data = {
        'type': 'FeatureCollection',
        'crs': {
            'type': 'name',
            'properties': {
                'name': 'urn:ogc:def:crs:OGC:1.3:CRS84'
            }
        },
        'features': []
    }
    counties = test_counties_year[year]
    for county in counties:
        entry_list = counties[county]
        for entry in entry_list:
            file_data['features'].append(entry.as_feature())
    with open(OUTPUT_LOCATION + str(year) + '_output_data.geojson', 'w') as f:
        f.write(json.dumps(file_data, indent=4, sort_keys=True))
