from shapely.geometry import Polygon
# from geojson import Point, Feature, Polygon

DATASET_LOCATION = './Datasets/'
OUTPUT_LOCATION = './Output/'
CSV_EXTENSION = '.csv'
GEO_JSON_EXTENSION = '.geojson'

CRIME_DATA_13 = DATASET_LOCATION + 'CrimeData_13' + CSV_EXTENSION
CRIME_DATA_14 = DATASET_LOCATION + 'CrimeData_14' + CSV_EXTENSION
CRIME_DATA_15 = DATASET_LOCATION + 'CrimeData_15' + CSV_EXTENSION
CRIME_DATA_16 = DATASET_LOCATION + 'CrimeData_16' + CSV_EXTENSION
CRIME_DATA_17 = DATASET_LOCATION + 'CrimeData_17' + CSV_EXTENSION
CRIME_DATA_18 = DATASET_LOCATION + 'CrimeData_18' + CSV_EXTENSION

LEAD_DATA_13 = DATASET_LOCATION + 'lead_93' + CSV_EXTENSION
LEAD_DATA_14 = DATASET_LOCATION + 'lead_94' + CSV_EXTENSION
LEAD_DATA_15 = DATASET_LOCATION + 'lead_95' + CSV_EXTENSION
LEAD_DATA_16 = DATASET_LOCATION + 'lead_96' + CSV_EXTENSION
LEAD_DATA_17 = DATASET_LOCATION + 'lead_97' + CSV_EXTENSION
LEAD_DATA_18 = DATASET_LOCATION + 'lead_98' + CSV_EXTENSION

LEAD_DATA_19 = DATASET_LOCATION + 'lead_99' + CSV_EXTENSION
LEAD_DATA_20 = DATASET_LOCATION + 'lead_00' + CSV_EXTENSION
LEAD_DATA_21 = DATASET_LOCATION + 'lead_01' + CSV_EXTENSION
LEAD_DATA_22 = DATASET_LOCATION + 'lead_02' + CSV_EXTENSION
LEAD_DATA_23 = DATASET_LOCATION + 'lead_03' + CSV_EXTENSION
LEAD_DATA_24 = DATASET_LOCATION + 'lead_04' + CSV_EXTENSION
LEAD_DATA_25 = DATASET_LOCATION + 'lead_05' + CSV_EXTENSION
LEAD_DATA_26 = DATASET_LOCATION + 'lead_06' + CSV_EXTENSION

COUNTY_COORDINATE_DATA = DATASET_LOCATION + 'Illinois_County_Maps' + GEO_JSON_EXTENSION

OUTPUT = OUTPUT_LOCATION + 'output' + CSV_EXTENSION
TEST_OUTPUT = OUTPUT_LOCATION + 'test_output' + CSV_EXTENSION

crime_years = {
    2013: CRIME_DATA_14,
    2014: CRIME_DATA_14,
    2015: CRIME_DATA_15,
    2016: CRIME_DATA_16,
    2017: CRIME_DATA_17,
    2018: CRIME_DATA_18
}

lead_years = {
    1993: LEAD_DATA_13,
    1994: LEAD_DATA_14,
    1995: LEAD_DATA_15,
    1996: LEAD_DATA_16,
    1997: LEAD_DATA_17,
    1998: LEAD_DATA_18
}

test_lead_years = {
    1999: LEAD_DATA_19,
    2000: LEAD_DATA_20,
    2001: LEAD_DATA_21,
    2002: LEAD_DATA_22,
    2003: LEAD_DATA_23,
    2004: LEAD_DATA_24,
    2005: LEAD_DATA_25,
    2006: LEAD_DATA_26
}

year_match = {
    2013: 1993,
    2014: 1994,
    2015: 1995,
    2016: 1996,
    2017: 1997,
    2018: 1998
}

class CrimeRecordEntry:
    def __init__(self, year, county, population, total_crimes, crime_rate, homicide, rape, robbery, assault, burglary, arson, human_trafficking, total_arrests, arrest_rate):
        self.year = year
        self.county = county
        self.population = population
        self.total_crimes = total_crimes
        self.crime_rate = crime_rate
        self.homicide = homicide
        self.rape = rape
        self.robbery = robbery
        self.assault = assault
        self.burglary = burglary
        self.arson = arson
        self.human_trafficking = human_trafficking
        self.total_arrests = total_arrests
        self.arrest_rate = arrest_rate

    def __str__(self):
        return str(self.year) + '\t' + str(self.county) + '\t' + str(self.population) + '\t' + str(self.total_crimes) + '\t' + str(self.crime_rate) + '\t' + str(self.homicide) + '\t' + str(self.rape) + '\t' + str(self.robbery) + '\t' + str(self.assault) + '\t' + str(self.burglary) + '\t' + str(self.arson) + '\t' + str(self.human_trafficking) + '\t' + str(self.total_arrests) + '\t' + str(self.arrest_rate)


class LeadRecordEntry:
    def __init__(self, year, county, total_tested, child_population, low_lead, medium_lead, high_lead):
        self.year = year
        self.county = county
        self.total_tested = total_tested
        self.child_population = child_population
        self.low_lead = low_lead
        self.medium_lead = medium_lead
        self.high_lead = high_lead

    def add_another_county(self, entry):
        self.total_tested += entry.total_tested
        self.child_population += entry.child_population
        self.low_lead += entry.low_lead
        self.medium_lead += entry.medium_lead
        self.high_lead += entry.high_lead

    def __str__(self):
        return str(self.year) + '\t' + str(self.county) + '\t' + str(self.total_tested) + '\t' + str(self.child_population) + '\t' + str(self.low_lead) + '\t' + str(self.medium_lead) + '\t' + str(self.high_lead)


class Geometry:
    def __init__(self, type, coordinates):
        self.type = type
        self.coordinates = coordinates[0]

    def centroid(self):
        polygon = Polygon(self.coordinates)
        return polygon.centroid.x, polygon.centroid.y

    def as_feature(self):
        return {
            "type": self.type,
            "coordinates": [self.coordinates]
        }


class Entry:
    def __init__(self, crime_entry, lead_entry):
        if crime_entry == None:
            self.county = lead_entry.county
            self.crime_year = 0
            self.population = 0
            self.total_crimes = 0
            self.crime_rate = 0
            self.homicide = 0
            self.rape = 0
            self.robbery = 0
            self.assault = 0
            self.burglary = 0
            self.arson = 0
            self.human_trafficking = 0
            self.total_arrests = 0
            self.arrest_rate = 0
            self.is_test = True
        else:
            self.county = crime_entry.county
            self.crime_year = crime_entry.year
            self.population = crime_entry.population
            self.total_crimes = crime_entry.total_crimes
            self.crime_rate = crime_entry.crime_rate
            self.homicide = crime_entry.homicide
            self.rape = crime_entry.rape
            self.robbery = crime_entry.robbery
            self.assault = crime_entry.assault
            self.burglary = crime_entry.burglary
            self.arson = crime_entry.arson
            self.human_trafficking = crime_entry.human_trafficking
            self.total_arrests = crime_entry.total_arrests
            self.arrest_rate = crime_entry.arrest_rate
            self.is_test = False
        self.poisoning_year = lead_entry.year
        self.total_tested = lead_entry.total_tested
        self.child_population = lead_entry.child_population
        self.low_lead = lead_entry.low_lead
        self.medium_lead = lead_entry.medium_lead
        self.high_lead = lead_entry.high_lead
        self.geometry = None
        self.latitude = 0.0
        self.longitude = 0.0

    def set_geometry(self, type, coordinates):
        self.geometry = Geometry(type, coordinates)
        self.latitude, self.longitude = self.geometry.centroid()

    def to_row(self):
        if self.is_test:
            return [self.county, self.latitude, self.longitude, self.poisoning_year, self.total_tested, self.child_population, self.low_lead, self.medium_lead, self.high_lead]
        return [self.county, self.latitude, self.longitude, self.crime_year, self.population, self.total_crimes, self.crime_rate, self.homicide, self.rape, self.robbery, self.assault, self.burglary, self.arson, self.human_trafficking, self.total_arrests, self.arrest_rate, self.poisoning_year, self.total_tested, self.child_population, self.low_lead, self.medium_lead, self.high_lead]

    def as_feature(self):
        if self.is_test:
            return {
                "type": "Feature",
                "properties": {
                    "county": self.county,
                    "poisoning_year": self.poisoning_year,
                    "low_lead": self.low_lead,
                    "medium_lead": self.medium_lead,
                    "high_lead": self.high_lead,
                    "total_poisoned": self.low_lead + self.medium_lead + self.high_lead
                },
                "geometry": self.geometry.as_feature()
            }
        return {
            "type": "Feature",
            "properties": {
                "county": self.county,
                "crime_year": self.crime_year,
                "poisoning_year": self.poisoning_year,
                "population": self.population,
                "crime_rate": self.crime_rate,
                "arrest_rate": self.arrest_rate,
                "low_lead": self.low_lead,
                "medium_lead": self.medium_lead,
                "high_lead": self.high_lead,
                "total_poisoned": self.low_lead + self.medium_lead + self.high_lead
            },
            "geometry": self.geometry.as_feature()
        }

    def __str__(self):
        return str(self.county) + '\t' + str(self.latitude) + '\t' + str(self.longitude) + '\t' + str(self.crime_year) + '\t' + str(self.population) + '\t' + str(self.total_crimes) + '\t' + str(self.crime_rate) + '\t' + str(self.homicide) + '\t' + str(self.rape) + '\t' + str(self.robbery) + '\t' + str(self.assault) + '\t' + str(self.burglary) + '\t' + str(self.arson) + '\t' + str(self.human_trafficking) + '\t' + str(self.total_arrests) + '\t' + str(self.arrest_rate) + '\t' + str(self.poisoning_year) + '\t' + str(self.total_tested) + '\t' + str(self.child_population) + '\t' + str(self.low_lead) + '\t' + str(self.medium_lead) + '\t' + str(self.high_lead)


class County:
    def __init__(self, name, lat, lang):
        self.name = name
        self.lat = lat
        self.lang = lang

    def as_feature(self):
        point = Point((self.lat, self.lang))
        return Feature(geometry=point, properties={"name": self.name})
