
# coding: utf-8

# All of the following code was initially coded in a Jupyter Notebook.

# In[1]:


import seaborn as sns
import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import zipfile
from pathlib import Path

# Default plot configurations
# get_ipython().magic('matplotlib inline')
plt.rcParams['figure.figsize'] = (16,8)
plt.rcParams['figure.dpi'] = 150
sns.set()


# In[2]:


data_dir = Path("../Output")


# In[3]:


data = pd.read_csv("./Output/training.csv")


# In[4]:


f = plt.figure()
sns.distplot(a=data["CRIME RATE"])
plt.show()
f.savefig("./Images/crime_rate_distribution")


# In[5]:


sns.scatterplot(x = ("LOW LEAD LEVELS (15-19 mcg/dl)"), y = "CRIME RATE", data = data)
plt.show()


# In[6]:


sns.scatterplot(x = ("MEDIUM LEAD LEVELS (20-44 mcg/dl)"), y = "CRIME RATE", data = data)
plt.show()


# In[7]:


sns.scatterplot(x = ("HIGH LEAD LEVELS (>45 mcg/dl)"), y = "CRIME RATE", data = data)
plt.show()


# In[8]:


data.loc[data["LOW LEAD LEVELS (15-19 mcg/dl)"] > 5000]


# In[9]:


data.loc[data["MEDIUM LEAD LEVELS (20-44 mcg/dl)"] > 3000]


# In[10]:


data.loc[data["HIGH LEAD LEVELS (>45 mcg/dl)"] > 200]


# In[11]:


noCook = data.loc[data["COUNTY"] != "COOK"]


# In[13]:


f = plt.figure()
sns.scatterplot(x = ("LOW LEAD LEVELS (15-19 mcg/dl)"), y = "CRIME RATE", data = noCook)
plt.show()
f.savefig("./Images/low_vs_crime")


# In[14]:


f = plt.figure()
sns.scatterplot(x = ("MEDIUM LEAD LEVELS (20-44 mcg/dl)"), y = "CRIME RATE", data = noCook)
plt.show()
f.savefig("./Images/med_vs_crime")


# In[15]:


f = plt.figure()
sns.scatterplot(x = ("HIGH LEAD LEVELS (>45 mcg/dl)"), y = "CRIME RATE", data = noCook)
plt.show()
f.savefig("./Images/high_vs_crime")


# In[16]:


# proportion of lead infected with number tested
data["INFECTED RATE"] = (data["LOW LEAD LEVELS (15-19 mcg/dl)"] + data["MEDIUM LEAD LEVELS (20-44 mcg/dl)"] + data["HIGH LEAD LEVELS (>45 mcg/dl)"])/data["TOTAL TESTED"]
data["CRIMERATE"] = data["TOTAL CRIMES"] / data["POPULATION"] *10


# In[18]:


g = sns.FacetGrid(data.loc[data["POPULATION"] > 100000], col = "COUNTY", col_wrap=5, height=2, xlim=(2013,2018))
g.map(sns.lineplot, "CRIME YEAR", "INFECTED RATE", color="b", ci=None);
g.map(sns.lineplot, "CRIME YEAR", "CRIMERATE", color="r", ci=None);
g.set(xlabel="CRIME YEAR", ylabel="RATES", xlim=(2012.5,2018.5))
g.set_xticklabels([2012,2015,2018])
g.add_legend()
plt.show()
g.savefig("./Images/crime_and_lead_rates")


# In[26]:


f = plt.figure()
sns.distplot(a=data["INFECTED RATE"])
plt.show()
f.savefig("./Images/infected_rate_distribution")


# In[28]:


data["TOTAL INFECTED"] = data["LOW LEAD LEVELS (15-19 mcg/dl)"] +                data["MEDIUM LEAD LEVELS (20-44 mcg/dl)"] +                data["HIGH LEAD LEVELS (>45 mcg/dl)"]


# In[29]:


sns.scatterplot(x = ("TOTAL INFECTED"), y = "CRIME RATE", data = data.loc[data["COUNTY"] != "COOK"])
plt.show()


# In[31]:


data['INFECTED RATE'] = (data["LOW LEAD LEVELS (15-19 mcg/dl)"] +                data["MEDIUM LEAD LEVELS (20-44 mcg/dl)"] +                data["HIGH LEAD LEVELS (>45 mcg/dl)"]) / data["TOTAL TESTED"]


# In[33]:


data.sort_values("TOTAL TESTED", ascending=False).head(15)

